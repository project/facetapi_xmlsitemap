<?php

function facetapi_xmlsitemap_settings_form($form) {
  $defaults = variable_get(FACETAPI_XMLSITEMAP_CRON_DATA, FALSE);

  $searchers = facetapi_get_searcher_info();
  if (empty($searchers)) {
    drupal_set_message(t('You have no active Facet API searchers.'), 'warning');
    return array();
  }

  $searchers_options = array();
  foreach ($searchers as $key => $info) {
    $searchers_options[$key] = $info['label'];
  }
  $form['searcher'] = array(
    '#type' => 'select',
    '#title' => t('Facet API Searcher'),
    '#options' => $searchers_options,
    '#default_value' => isset($defaults['searcher']) ? $defaults['searcher'] : '',
  );

  if (isset($defaults['searcher'])) {
    $facets = facetapi_get_enabled_facets($defaults['searcher']);
    sort($facets);

    $facet_options = array();
    foreach ($facets as $facet) {
      $facet_options[$facet['name']] = format_string(
        '@tech [@type] - <strong>@label</strong>',
        array(
          '@tech' => $facet['name'],
          '@type' => $facet['field type'],
          '@label' => $facet['label']
        ));
    }

    $form['facets'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Starting facets'),
      '#description' => t('Facets which values will be used as a starting level for sitemap links search'),
      '#options' => $facet_options,
      '#default_value' => isset($defaults['facets']) ? $defaults['facets'] : array(),
    );

    $form['exclude'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Excluded facets'),
      '#description' => t('Facets to be totally ignored during sitemap search'),
      '#options' => $facet_options,
      '#default_value' => isset($defaults['exclude']) ?  $defaults['exclude'] : array(),
    );

    $priority_options = array();
    for ($i = 0; $i < 10; $i++) {
      $priority_options['0.' . $i] = '0.' . $i;
    }

    $form['sitemap_priority'] = array(
      '#type' => 'select',
      '#title' => t('XMLSitemap Entry Priority'),
      '#options' => $priority_options,
      '#default_value' => array($defaults['sitemap_priority'] ?: XMLSITEMAP_PRIORITY_DEFAULT),
    );

    $changefreq_options = xmlsitemap_get_changefreq_options();
    $form['sitemap_changefreq'] = array(
      '#type' => 'select',
      '#title' => t('XMLSitemap Entry Frequency'),
      '#options' => $changefreq_options,
      '#default_value' => array($defaults['sitemap_changefreq'] ?: XMLSITEMAP_FREQUENCY_MONTHLY),
    );

    $form['num_entries_at_once'] = array(
      '#type' => 'textfield',
      '#title' => t('Number of entries in batch'),
      '#size' => 3,
      '#description' => t('As we perform links search in batch mode, this value determines how many links would be check in one batch'),
      '#default_value' => isset($defaults['num_entries_at_once']) ? $defaults['num_entries_at_once'] : 50,
    );

    $form['save_for_cron'] = array(
      '#type' => 'checkbox',
      '#title' => t('Save settings for cron run'),
      '#description' => t('Uncheck to execute full links search now'),
      '#default_value' => TRUE,
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => isset($defaults['searcher']) ? t('Save (And Run)') : t('Save'),
  );

  return $form;
}

/**
 * @param $form
 * @param $form_state
 */
function facetapi_xmlsitemap_settings_form_submit($form, &$form_state) {
  $form_state['values']['num_levels'] = count($form['exclude']['#options']) - count(array_filter($form_state['values']['exclude']));

  variable_set(FACETAPI_XMLSITEMAP_CRON_DATA, $form_state['values']);

  if (isset($form_state['values']['save_for_cron'])) {
    if ($form_state['values']['save_for_cron']) {
      drupal_set_message('Batch settings saved to be executed with Cron.');
    }
    else {
      batch_set(facetapi_xmlsitemap_batch_init($form_state['values'], FALSE));
    }
  }
}
